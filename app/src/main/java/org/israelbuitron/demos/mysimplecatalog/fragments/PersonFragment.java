package org.israelbuitron.demos.mysimplecatalog.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.israelbuitron.demos.mysimplecatalog.R;
import org.israelbuitron.demos.mysimplecatalog.adapters.MyPersonRecyclerViewAdapter;
import org.israelbuitron.demos.mysimplecatalog.beans.Person;
import org.israelbuitron.demos.mysimplecatalog.dummy.DummyContent;

import java.util.List;

/**
 * A fragment representing a list of people.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 * @author Israel Buitron
 */
public class PersonFragment extends Fragment {

    // Argument name of column count
    private static final String ARG_COLUMN_COUNT = "column-count";

    // Column count. Default 1.
    private int mColumnCount = 1;

    // Callback listener
    private OnListFragmentInteractionListener mListener;

    public PersonFragment() {
        // Empty constructor
    }

    @SuppressWarnings("unused")
    public static PersonFragment newInstance(int columnCount) {
        // Fragment instance
        PersonFragment fragment = new PersonFragment();

        // Set argument for fragment instance
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Process arguments if exist
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate fragment layout
        View view = inflater.inflate(R.layout.fragment_person_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            // Get layout context
            Context context = view.getContext();

            // Use view as RecyclerView
            RecyclerView recyclerView = (RecyclerView) view;

            // Decide layout manager
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            // Set adapter for view
            List<Person> people = DummyContent.PEOPLE_LIST;
            RecyclerView.Adapter adapter = new MyPersonRecyclerViewAdapter(people, mListener);
            recyclerView.setAdapter(adapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Person item);
    }
}
