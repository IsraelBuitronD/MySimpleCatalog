package org.israelbuitron.demos.mysimplecatalog.dummy;

import org.israelbuitron.demos.mysimplecatalog.beans.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) people.
     */
    public static final List<Person> PEOPLE_LIST = new ArrayList<Person>();

    /**
     * A map of sample (dummy) people, by ID.
     */
    public static final Map<Integer, Person> PEOPLE_MAP = new HashMap<Integer, Person>();

    static {
        addItem(new Person(1, "Alfonso García Robles"));
        addItem(new Person(2, "Octavio Paz Lozano"));
        addItem(new Person(3, "Mario Molina Enriquez"));
    }

    private static void addItem(Person person) {
        PEOPLE_LIST.add(person);
        PEOPLE_MAP.put(person.getId(), person);
    }
}
